# Agent-based model

Scripts to run an agent-based model simulation of cooperation and trust with tags for migration decisions.

## Installation

Requires [Mesa](https://github.com/projectmesa/mesa).
For all requirements, see ``requirements.txt``

```bash
pip install -r requirements.txt
```

## Usage
```bash
python simulation.py @config_file
```

See ``simulation.py`` for the format of the config files. To submit the simulation job on an HPC, see ``submit-jobs.py``.

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)
